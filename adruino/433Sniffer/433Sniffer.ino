#include <RCSwitch.h>

RCSwitch rcSwitch = RCSwitch();

void setup() {
  Serial.begin(115200);
  rcSwitch.enableReceive(12);
}

void loop() {
  if (rcSwitch.available()) {
    Serial.print("Received ");
    Serial.print(rcSwitch.getReceivedValue());
    Serial.print(" / ");
    Serial.print(rcSwitch.getReceivedBitlength());
    Serial.print("bit ");
    Serial.print("Protocol: ");
    Serial.println(rcSwitch.getReceivedProtocol());
    Serial.print("PulseLength: ");
    Serial.println(rcSwitch.getReceivedDelay());

    rcSwitch.resetAvailable();
  }
}