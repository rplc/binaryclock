#include <digoo.h>
#include <homeGW.h>

HomeGW gw(1); // 1 = number of plugins (only if different needed)
digoo digooReceiver;

#define RF_RECEIVER_PIN 12

void setup() {
  Serial.begin(115200);

  pinMode(RF_RECEIVER_PIN, OUTPUT);
  digitalWrite(RF_RECEIVER_PIN, LOW);
  pinMode(RF_RECEIVER_PIN, INPUT);
  digitalWrite(RF_RECEIVER_PIN, LOW);
  gw.setup(RF_RECEIVER_PIN);
 
  gw.registerPlugin(&digooReceiver);
}

void loop() { 
  uint64_t p = 0;
  String id;

  if(digooReceiver.available() && (p = digooReceiver.getPacket())) {
    //TODO check if is valid temp or so... but even then i will get some garbage...
    //make more robust with https://github.com/aquaticus/nexus433#nexus-protocol
    if (digooReceiver.isValidPacket(p) == 0 && digooReceiver.isValidWeather(p) == 0) {
      Serial.print("Digoo: ");
      Serial.println(digooReceiver.getString(p));
      Serial.print("ID: ");
      Serial.println(digooReceiver.getId(p));
      Serial.print("channel: ");
      Serial.println(digooReceiver.getChannel(p));
      // battery values: 0/1
      Serial.print("battery: ");
      Serial.println(digooReceiver.getBattery(p));
      Serial.print("temperature: ");
      Serial.println(digooReceiver.getTemperature(p));
      Serial.print("humidity: ");
      Serial.println(digooReceiver.getHumidity(p));
    }
  }

  delay(1000);
}
