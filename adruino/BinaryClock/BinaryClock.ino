#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#define FASTLED_INTERRUPT_RETRY_COUNT 0
#define FASTLED_ALLOW_INTERRUPTS 0
#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>
#include <RCSwitch.h>

const char *ssid = "no place like 127.0.0.1 - iot";
const char *password = "***";

byte displayHour1;
byte displayHour2;
byte displayMin1;
byte displayMin2;
byte displayState;
byte displayAlert;
bool wasAlert = false;

#define CLOCK_STRIP_COUNT 13
#define CLOCK_STRIP_PIN 14

#define LIGHT_STRIP_COUNT 47
#define LIGHT_STRIP_PIN 13

#define PIN_433 5

WiFiClient espClient;
PubSubClient client(espClient);
CRGB clock_strip[CLOCK_STRIP_COUNT];
// light_strip currently not in use
//CRGB light_strip[LIGHT_STRIP_COUNT];
RCSwitch rcSwitch = RCSwitch();

// timing
const int mqttInterval = 200;
const int ledInterval = 500;
unsigned long prevMqttMillis = 0;
unsigned long prevLedMillis = 0;

void setup() {
  Serial.begin(115200);

  Serial.println();
  Serial.print("Connecting to ");
  Serial.print(ssid);
  Serial.print(" with mac ");
  Serial.print(WiFi.macAddress());

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer("192.168.5.212", 1883);
  client.setCallback(callback);

  FastLED.addLeds<WS2812B, CLOCK_STRIP_PIN, GRB>(clock_strip, CLOCK_STRIP_COUNT);
  //FastLED.addLeds<WS2812B, LIGHT_STRIP_PIN, GRB>(light_strip, LIGHT_STRIP_COUNT);

  rcSwitch.enableTransmit(PIN_433);
  rcSwitch.setProtocol(1);
  rcSwitch.setPulseLength(248);
}

void loop() {
  long currentMillis = millis();

  if (currentMillis - prevMqttMillis > mqttInterval) {
    prevMqttMillis = currentMillis;
    if (!client.connected()) {
      reconnect();
    }
    client.loop();
  }

  if (currentMillis - prevLedMillis > ledInterval) {
    prevLedMillis = currentMillis;
    updateDisplay();
    // keep the show in the main loop and just set the led colors in the sub methods (just for easier control)
    FastLED.show();
  }
}

void callback(char *topic, byte *payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  if (length == 0) {
    Serial.println();
    return;
  }

  if (strcmp(topic, "espBinaryClock/control") == 0) {
    displayHour1 = payload[1];
    displayHour2 = payload[2];
    displayMin1 = payload[3];
    displayMin2 = payload[4];
    displayState = payload[5];
    displayAlert = payload[6];

    Serial.print((payload[0]-0x30));
    Serial.print((payload[1]-0x30));
    Serial.print((payload[2]-0x30));
    Serial.print((payload[3]-0x30));
    Serial.print((payload[4]-0x30));
    Serial.print((payload[5]-0x30));
    Serial.print((payload[6]-0x30));
    Serial.println();
    return;
  }

  if (strcmp(topic, "espBinaryClock/433payload") == 0) {
    long decimal = 0;
    for(int i = 0; i < length; i++) {
      decimal = decimal * 10 + (payload[i] - '0'); // convert to int...
    }

    if (decimal != 0) {
      rcSwitch.send(decimal, 24);    
    }
    Serial.println(decimal);
    return;
  }
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESPBinaryclock_strip-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");

      client.subscribe("espBinaryClock/control");
      client.subscribe("espBinaryClock/433payload");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void updateDisplay() {
  fill_solid(clock_strip, CLOCK_STRIP_COUNT, CRGB::Black);

  if ((displayState-0x30) == 0) {
    // all off
    return;
  }

  if ((displayAlert-0x30) == 1) {
    if (!wasAlert) {
      fill_solid(clock_strip, CLOCK_STRIP_COUNT, CRGB(0x420807));
    }
    wasAlert = !wasAlert;
  }

  displayNumber((displayHour1-0x30), 0);
  displayNumber((displayHour2-0x30), 2);

  displayNumber((displayMin1-0x30), 6);
  displayNumber((displayMin2-0x30), 9);
}

void displayNumber(uint8_t value, uint8_t ledOffset) {
  if (value & 1) {
    clock_strip[ledOffset] = CRGB(0x190e03);
  }
  if (value & 2) {
    clock_strip[ledOffset+1] = CRGB(0x2d1a05);
  }
  if (value & 4) {
    clock_strip[ledOffset+2] = CRGB(0x422607);
  }
  if (value & 8) {
    clock_strip[ledOffset+3] = CRGB(0x502d09);
  }
}
