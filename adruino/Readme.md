# Controlling WS2812B (NeoPixel) via ESP8266 Board
- **Pins are not numbered as labelled**
  - D2 is PIN 4 in Arduino!!!
- Do NOT use D3 (Arduino PIN 0)!!!

![PinOut](wemos_d1_mini_pinout.png)

- Components: Wemos D1 (ESP8266 ESP-12), Level-Shifter (Bi-directional Logic Level Shifter Converter Module 5V-3.3V), WS2812B LED Strip

- Connection Power (5V)
  - ESP8266 5V, GND
  - Level Shifter High Side: High Level, GND
  - LED Strip: 5V, GND
- Connection ESP8266 to Level Shifter
  - LED-Controlling-PIN to Low-Level-1, GND to GND, 3.3V to Low Level (next to Ground)
- Connection Level Shifter to LEDs
  - From PSU/Level Shifter High Level, GND to 5V, GND
  - High Level 1 to DIN

![Wiring](BinaryClock_wiring.png)

# Project ideas
- Hours : Minutes
- AM2320 temperature sensor
- different modi -> watch for udp/tcp message to switch modi?
  - mode 1: time display
  - mode 2: time display but color of hours indicate temperature (blue cold, yellow ok, red hot) and minutes color indicates humidity
  - mode 3: display 5 secs temperature and 5 secs humidity (perhaps api request to wetter.com (or so) to get outside temparature)

# Arduino installation Linux
- download Arduino IDE
- Arduino Preferences - additional Boards Manager URL: http://arduino.esp8266.com/stable/package_esp8266com_index.json
- Boards Manager - install ESP8266
- Tools -> Board
  - Generic ESP8266 Module
  - Reset Methode nodemcu
- install Libaries (NTPClient, Adafruit_NeoPixel, Adafruit Unified Sensor, Adafruit AM2320, https://github.com/dgomes/homeGW)
- sudo usermod -a -G dialout USERNAME
- log out, log in

# API
**Control request via UDP Port 1447**
```
payload
{
  "action": "control",
  "mode": "clock"
}
response
{
  "status": "ok"
}
```

**Temperature request**
```
payload
{
  "action": "getTemperature"
}
response
{
  "status": "ok",
  "inside": {
    "temp": "20°C",
    "hum": "50%"
  },
  "ch1": {
    "temp": "20°C",
    "hum": "50%"
  },
  "ch2": {
    "temp": "20°C",
    "hum": "50%"
  },
  "ch3": {
    "temp": "20°C",
    "hum": "50%"
  }
}
```