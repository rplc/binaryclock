drawClock = function() {
    const date = new Date(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();

    $('.dot').removeClass('on');

    displayInt('#h-1-', Math.floor(hours/10));
    displayInt('#h-2-', hours%10);
    $('#h').html(pad(hours));

    displayInt('#m-1-', Math.floor(minutes/10));
    displayInt('#m-2-', minutes%10);
    $('#m').html(pad(minutes));

    displayInt('#s-1-', Math.floor(seconds/10));
    displayInt('#s-2-', seconds%10);
    $('#s').html(pad(seconds));

    setTimeout(drawClock, 1000);
}

pad = function(number) {
    return ('0'+number).slice(-2); 
}

displayInt = function(prefix, number) {
    number&1 && $(prefix + '1').addClass('on');
    number&2 && $(prefix + '2').addClass('on');
    number&4 && $(prefix + '4').addClass('on');
    number&8 && $(prefix + '8').addClass('on');
}

drawClock();